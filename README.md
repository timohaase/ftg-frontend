# FTG-FrontEnd

## Project name
Fair City Guide

## Description
The Fair City Guide is an App which offers sustainable businesses a platform and connects them to people interested in sustainable life choices.
I have developed the app as part of my master thesis at the University of Muenster and in cooperation with Muenster.Fair and Fairer Handel Muenster:

University Of Muenster, Practical Computer Science Group: https://www.wi.uni-muenster.de/de/institut/pi

Muenster.Fair: https://muensterfair.de/

Fairer Handel Muenster: https://fairtradestadtmuenster.de/

## About this repository
This repository contains the app frontend.
You can find the sourcecode in the "branches" section of this repository.

The Fair City Guide Frontend is written in React Native.
The Fair City Guide Backend is written in Spring Boot.
The app runs on iOS and Android.

## License
GPL v3

## Project status
Published on Googly Play Store and iOS App Store.

For more information check the project description pages:
* Description on DIGIFARM.MS: https://www.stadt-muenster.de/digifarm/projekte/newsdetail/fair-city-guide-muenster
* Infos on Fair Trade City Münster: https://fairtradestadtmuenster.de/fair-city-guide-muenster-app



